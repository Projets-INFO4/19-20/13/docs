12/02:
RDV avec un membre de la DGD DAPAL pour définir les besoins

17/02:
Tests divers avec l'interface grafana, création des dashboards nécessaires

02/03:
1ère mise en page de l'interface

03/03:
Envoi de l'interface à la DGD DAPAL pour avoir un premier retour

9-10/03:
Modification des couleurs indicatifs des panels et soutenance
Ajout d'une vue globale et modification de la formule de calcul de la température ressentie

16/03:
Ajout d'une moyenne journalière de la température ressentie.

30/03:
Ajout d'une vue pour les capteurs EMS

07/04:
Mise à jour du dashboard Siconia
Préparation pour la présentation